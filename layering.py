import svgutils.transform as st
import xml.etree.ElementTree as ET
import os, argparse
parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", help="file name")
args = parser.parse_args()
name = args.name

#### coloring each layer
## red layer
tree = ET.parse("r.svg")
root = tree.getroot()
for child in root:
    child.set('style','stroke:rgb(255,0,0);fill:none;')
tree.write('r.svg')
## green layer
tree = ET.parse("g.svg")
root = tree.getroot()
for child in root:
    child.set('style','stroke:rgb(201,47,22);fill:none;')
tree.write('g.svg')
## blue layer
tree = ET.parse("b.svg")
root = tree.getroot()
for child in root:
    child.set('style','stroke:rgb(141,22,201);fill:none;')
tree.write('b.svg')

### merge the image
base = st.fromfile('r.svg')
second_svg = st.fromfile('g.svg')
third_svg = st.fromfile('b.svg')
base.append(second_svg)
base.append(third_svg)
base.save(name+'_merged.svg')
